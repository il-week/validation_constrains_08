package net.tncy.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ISBNValidator implements ConstraintValidator<ISBN, String> {

    @Override
    public void initialize(ISBN constraintAnnotation) {
        // Ici le validateur peut accéder aux attribut de l’annotation.
    }

    @Override
    public boolean isValid(String bookNumber, ConstraintValidatorContext constraintContext) {
        boolean valid = false;
			// Algorithme de validation du numéro ISBN
            if (bookNumber.length() == 13 && bookNumber.matches("[0-9]*") ) {
                valid = true;
            }
			return valid;
}

    
}
